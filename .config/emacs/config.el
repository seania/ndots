(setq auto-save-default nil)
(setq create-lockfiles nil)

(defvar backupdir (expand-file-name "backups" user-emacs-directory))
(unless (file-exists-p backupdir)
  (mkdir backupdir))
(setq backup-directory-alist '(("." . "~/.config/emacs/backups")))

(setq backup-by-copying t) ;; Do not destroy hardlinks
(setq version-control t) ;; Add versions to backups

(defalias 'yes-or-no-p 'y-or-n-p)

(setq tab-width 2)
(setq indent-tabs-mode nil)

(set-face-attribute 'default nil :family "JetBrains Mono Nerd Font")
(set-face-attribute 'default nil :height 140) ;; 14pt

(recentf-mode 1)
(save-place-mode 1)

(defun rc/add-exec-path (p)
  (defvar path (expand-file-name p))
  (when (file-exists-p path)
    (add-to-list 'exec-path path)))
  
(rc/add-exec-path "~/.roswell/bin")
(rc/add-exec-path "~/.local/bin")
(rc/add-exec-path "~/dotfiles/scripts")

(setq-default custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load-file custom-file))

(use-package emacs
  :init
  (setq tab-always-indent 'complete)
  (setq completion-cycle-threshold nil))

(use-package clojure-mode) ;; Clojure: JVM Lisp.
(use-package lua-mode) ;; Lua: The language for extensions
(use-package rust-mode) ;; Rust: Fearless Concurrency
(use-package zig-mode) ;; Zig: Best C toolchain
(use-package haskell-mode) ;; Haskell: Pretty nice for scripts.
(use-package js2-mode) ;; JavaScript: Unholy abomination of a language, only next to C++
(use-package fish-mode) ;; FISH: Friendly Interactive SHell
(use-package go-mode) ;; Go: Thompson
(use-package nix-mode) ;; Nix: Nix: Nix: Nix: Nix...

(use-package web-mode
  :config
  (use-package emmet-mode
    :config
    (add-hook 'web-mode-hook 'emmet-mode))
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode)))

(use-package tree-sitter
  :config
  (use-package tree-sitter-langs)
  (add-hook 'c-mode-hook 'tree-sitter-hl-mode)
  (add-hook 'rust-mode-hook 'tree-sitter-hl-mode)
  (add-hook 'clojure-mode-hook 'tree-sitter-hl-mode)
  (add-hook 'lua-mode-hook 'tree-sitter-hl-mode)
  (add-hook 'haskell-mode-hook 'tree-sitter-hl-mode)
  (add-hook 'zig-mode-hook 'tree-sitter-hl-mode)
  (add-hook 'nix-mode-hook 'tree-sitter-hl-mode))

(use-package flycheck
  :config
  (add-hook 'prog-mode-hook 'flycheck-mode))

(setq-default display-line-numbers-type 'relative)
(global-display-line-numbers-mode 1)

(defun rc/prog-hook ()
  (interactive)
  (electric-pair-local-mode 1)
  (electric-indent-local-mode 1))
(add-hook 'prog-mode-hook 'rc/prog-hook)

(use-package rainbow-delimiters
  :hook ((lisp-mode . rainbow-delimiters-mode)
	 (clojure-mode . rainbow-delimiters-mode)
	 (emacs-lisp-mode . rainbow-delimiters-mode)
	 (scheme-mode . rainbow-delimiters-mode)))

(delete-selection-mode 1)
(global-display-line-numbers-mode 1)
(global-set-key [escape] 'keyboard-escape-quit)

(use-package vertico :config (vertico-mode 1)) ;; Better Interactive Completion
(use-package marginalia :config (marginalia-mode 1)) ;; Adds Completions to some Commands
(use-package consult) ;; Kinda Like Neovim's Telescope
(use-package general) ;; Keybinds

(use-package evil ;; VI Emulation
  :init
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1)
  (use-package evil-collection)
  (use-package evil-nerd-commenter
    :config
    (general-define-key
     :keymaps '(normal visual)
     "gc" 'evilnc-comment-or-uncomment-lines)
    (global-set-key (kbd "M-;") 'evilnc-comment-or-uncomment-lines)))

(use-package company ;; Completion Engine
  :config
  (global-company-mode 1))
(use-package which-key :config (which-key-mode 1)) ;; Keybind Autocompletion

(use-package magit) ;; Git Client
(use-package vterm ;; Terminal Emulator
  :config
  (add-hook 'vterm-mode-hook (lambda () (evil-local-mode 0))))

(use-package yasnippet ;; Snippet engine
  :config
  (yas-reload-all)
  (yas-global-mode))

(use-package projectile ;; Project Interaction
  :config
  (projectile-mode 1))

(use-package async
  :config 
  (dired-async-mode 1))

(use-package org
  :config
  (use-package org-modern
    :config
    (add-hook 'org-mode-hook 'org-modern-mode)))


;; Fix org-indent-mode with org-modern
(use-package org-modern-indent 
  :straight (org-modern-indent 
	     :type git 
	     :host github 
	     :repo "jdtsmith/org-modern-indent")
  :config
  ;;(add-hook 'org-mode-hook #'org-modern-indent-mode 90)
  (setq org-startup-indented nil))

;; Electric Indent messes up code indenting in Org Mode
(defun rc/fix-org-code-indent ()
  (electric-indent-local-mode 0))
(add-hook 'org-mode-hook 'rc/fix-org-code-indent)

(use-package toc-org
  :commands toc-org-enable
  :init (add-hook 'org-mode-hook 'toc-org-enable))

(use-package restart-emacs
  :config
  (general-define-key
   :keymaps '(normal visual emacs)
   "M-r" 'restart-emacs))

(use-package nerd-icons)
(use-package nerd-icons-dired
  :config
  (add-hook 'dired-mode-hook 'nerd-icons-dired-mode))

(when (display-graphic-p)
  (scroll-bar-mode 0))
(tool-bar-mode 0)
(menu-bar-mode 0)
(fringe-mode 0)

;;(setq-default mode-line-position '("[%l,%c]"))
;;(setq-default mode-line-format '("%F %b %+ " mode-name " " mode-line-position " " evil-mode-line-format))
(use-package doom-modeline
  :config
  (doom-modeline-mode 1))

(use-package gruber-darker-theme)
(use-package modus-themes)
(use-package doom-themes)
(use-package solarized-theme)
(use-package zenburn-theme)
(use-package hc-zenburn-theme)
(use-package nordic-night-theme)
(use-package kaolin-themes)
(use-package almost-mono-themes)

(defun rc/disable-themes ()
  (interactive)
  (dolist (th custom-enabled-themes)
    (disable-theme th)))

(rc/disable-themes)
(load-theme 'doom-nord t)

(use-package dashboard
  :config
  (setq dashboard-banner-logo-title (format "Welcome to Emacs, %s!" (getenv "USER")))
  (setq dashboard-startup-banner (expand-file-name "~/dotfiles/res/other/Ralsei.png"))
  
  (setq dashboard-center-content t)
  (setq dashboard-display-icons-p t)
  (setq dashboard-icon-type 'nerd-icons)
  
  (dashboard-setup-startup-hook))

(global-hl-line-mode 1)
(global-visual-line-mode 1)

(general-create-definer rc/keys!
  :keymaps '(normal visual emacs)
  :prefix "SPC"
  :global-prefix "C-c")

(general-create-definer rc/mode-keys!
  :keymaps '(normal insert visual emacs)
  :prefix "C-c"
  :global-prefix "C-c")

(rc/keys!
  "C-SPC" 'complete-symbol
  ;; Find
  "ff" 'consult-find
  "fm" 'consult-man
  "fc" 'consult-theme
  "fb" 'consult-buffer
  "fg" 'consult-grep
  "fB" 'consult-bookmark
  
  ;; Git
  "gs" 'magit-stage-file
  "gS" 'magit-stage-modified
  "gc" 'magit-commit
  "gg" 'magit-status
  "gp" 'magit-push
  "gP" 'magit-pull
  
  ;; LSP
  "lr" 'lsp-rename
  "ld" 'lsp-find-definition
  "lR" 'lsp-find-references
  
  ;; Edit
  "ef" 'find-file
  "es" 'scratch-buffer
  
  ;; Boomarks
  "ba" 'bookmark-set
  "bd" 'bookmark-delete
  "bf" 'consult-bookmark

  ;; Switch To
  "sd" 'dashboard-open)

(rc/mode-keys! c-mode-map
  "mc" 'compile
  "md" 'gdb)

(rc/mode-keys! clojure-mode-map
  "mc" 'cider-connect-clj
  "ml" 'cider-load-file
  "mL" 'cider-load-and-switch-to-repl-buffer
  "ms" 'cider-switch-to-repl-buffer)

(rc/mode-keys! lisp-mode-map
  "mc" 'sly-connect
  "ml" 'sly-load-file
  "me" 'sly-eval-defun
  "mE" 'sly-eval-region
  "mq" 'sly-quit-lisp)

(use-package cider)

(setq inferior-lisp-program "ros run")
(use-package sly)

(use-package lsp-mode
  :commands lsp)
(use-package lsp-ui
  :commands lsp-ui-mode)

(use-package dap-mode)

(use-package rustic
  :config
  (setq rustic-lsp-server 'rust-analyzer)
  (setq rustic-lsp-client 'lsp-mode))

(use-package flycheck-rust
  :config
  (with-eval-after-load 'rust-mode
    (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)))

(use-package lsp-haskell)
(add-hook 'haskell-mode-hook #'lsp)
(add-hook 'haskell-literate-mode-hook #'lsp)

(add-hook 'c-mode-hook #'lsp)

(add-hook 'go-mode-hook #'lsp)

(use-package nerd-icons-dired
  :hook
  (dired-emode . nerd-icons-dired-mode))

(defun rc/dired-hook ()
  (evil-local-mode -1))
(add-hook 'dired-mode-hook 'rc/dired-hook)

(defun rc/eww-hook ()
  (local-set-key (kbd "C-c <left>") 'eww-back-url)
  (local-set-key (kbd "C-c <right>") 'eww-forward-url))
(add-hook 'eww-mode-hook 'rc/eww-hook)

(use-package pdf-tools
  :mode "\\.pdf\\'"
  :commands (pdf-loader-install)
  :bind (:map pdf-view-mode-map
	      ("j" . pdf-view-next-line-or-next-page)
              ("k" . pdf-view-previous-line-or-previous-page)
              ("C-=" . pdf-view-enlarge)
              ("C--" . pdf-view-shrink))
  :init (pdf-loader-install))

(defun rc/pdf-hook ()
  (display-line-numbers-mode -1)
  (blink-cursor-mode -1)
  (doom-modeline-mode -1))

(add-hook 'pdf-view-mode-hook 'rc/pdf-hook)
