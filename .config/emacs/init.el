(load-file (expand-file-name "package.el" user-emacs-directory))
(straight-use-package 'org)
(org-babel-load-file (expand-file-name "config.org" user-emacs-directory))

;; (defun rc/eww-hook ()
;;   "Hook for eww-mode"
;;   (local-set-key (kbd "C-c <left>") 'eww-back-url)
;;   (local-set-key (kbd "C-c <right>") 'eww-forward-url))
;; (add-hook 'eww-mode-hook 'rc/eww-hook)

