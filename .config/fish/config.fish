sh /etc/profile

export PATH="$PATH:$HOME/dotfiles/scripts:$HOME/dotfiles/wmscripts"

export EDITOR="emacs"
export VISUAL="emacs"

[ -d "/usr/share/glib-2.0/schemas" ] && export GSETTINGS_SCHEMA_DIR="/usr/share/glib-2.0/schemas"

[ -d "$HOME/.local/bin" ] && export PATH="$PATH:$HOME/.local/bin"
[ -d "$HOME/.roswell/bin" ] && export PATH="$PATH:$HOME/.roswell/bin"
[ -f "$HOME/.cargo/env" ] && source "$HOME/.cargo/env"

if test -d "/var/lib/flatpak/exports/share" && test -d "$HOME/.loca/share/flatpak/exports/share"
    export XDG_DATA_DIRS="$XDG_DATA_DIRS:/var/lib/flatpak/exports/share:$HOME/.local/share/flatpak/exports/share"
end

if status is-interactive
    alias ls="ls --color -lah"
    alias rm="rm -rI"
    alias grep="grep --color"
    alias fetch="fastfetch"

    function fish_prompt
      set user "$(set_color green)$(whoami)$(set_color normal)"
      set host "$(set_color blue)$(prompt_hostname)$(set_color normal)"
      set ppwd "$(set_color green)$(prompt_pwd)$(set_color normal)"
      echo "[$user@$host $ppwd]\$ "
    end

    fetch
end
