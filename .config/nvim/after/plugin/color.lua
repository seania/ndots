local opts = require("pyu.options")

opts.opts({
  termguicolors = true,
  background = "dark"
})

vim.g.everforest_background = "hard"
vim.g.everforest_enable_italic = true
vim.g.everforest_transparent_background = 2

vim.cmd("colo rose-pine")
