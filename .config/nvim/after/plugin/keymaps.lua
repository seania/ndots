local opts = require("pyu.options")

vim.cmd([[
imap <silent><expr> <Tab> luasnip#expand_or_jumpable() ? '<Plug>luasnip-expand-or-jump' : '<Tab>' 
snoremap <silent> <Tab> <cmd>lua require('luasnip').jump(1)<Cr>
snoremap <silent> <S-Tab> <cmd>lua require('luasnip').jump(-1)<Cr>
]])

opts.keymap.leader("n", "pf", ":Telescope find_files<CR>")
opts.keymap.leader("n", "pg", ":Telescope live_grep<CR>")

opts.keymap.leader("n", "la", ":Lspsaga code_action<CR>")
opts.keymap.leader("n", "lf", ":Lspsaga finder<CR>")
opts.keymap.leader("n", "ld", ":Lspsaga hover_doc<CR>")
opts.keymap.leader("n", "lp", ":Lspsaga peek_definition<CR>")

opts.keymap.leader("n", "fb", ":Telescope buffers<CR>")
opts.keymap.leader("n", "fm", ":Telescope man_pages<CR>")

