local opts = require("pyu.options")

local tabs = 2

opts.opts({
  tabstop = tabs,
  shiftwidth = tabs,
  softtabstop = tabs,
  expandtab = true
})

opts.opts({
  number = true,
  relativenumber = true
})
