local function opts(t, v)
  if type(t) == "string" then
    vim.opt[t] = v
    return
  end
  for k, v in pairs(t) do
    vim.opt[k] = v
  end
end

vim.g.mapleader = " " -- space as leader key

local function leader(mode, lhs, rhs, opts)
  vim.keymap.set(mode, "<leader>"..lhs, rhs, opts)
end

return {
  opts = opts,
  keymap = {
    set = vim.keymap.set,
    del = vim.keymap.del,
    leader = leader
  }
}
