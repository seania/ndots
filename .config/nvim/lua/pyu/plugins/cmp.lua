local cmp = require("cmp")
cmp.setup({
  snippet = {
    expand = function(args)
      require("luasnip").lsp_expand(args.body)
    end
  },
  window = {
    completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered()
  },
  mapping = cmp.mapping.preset.insert({
    ["<CR>"] = cmp.mapping.confirm({select = true}),
    ["<C-c>"] = cmp.mapping.abort(),
    ["<C-Space>"] = cmp.mapping.complete()
  }),
  sources = cmp.config.sources({
    {name = "nvim_lsp"},
    {name = "luasnip"},
    {name = "conjure"}
  }, {
    {name = "buffer"}
  })
})


