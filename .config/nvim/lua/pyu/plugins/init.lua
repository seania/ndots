---@diagnostic disable: missing-fields
---@diagnostic disable: undefined-field

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local function plugins(fn)
  local plugs = {}

  local function use(repo, opts)
    local o = opts or {}
    table.insert(o, 1, repo)
    table.insert(plugs, o)
  end

  fn(use)

  require("lazy").setup(plugs, {})
end

plugins(function(use)
  do -- libraries
    use("nvim-tree/nvim-web-devicons")
    use("nvim-lua/plenary.nvim")
    use("MunifTanjim/nui.nvim")
    use("rcarriga/nvim-notify")
  end

  use("nvim-treesitter/nvim-treesitter", {build = ":TSUpdate"})
  use("nvim-telescope/telescope.nvim")
  use("L3MON4D3/LuaSnip")
  use("folke/which-key.nvim")
  use("lukas-reineke/indent-blankline.nvim")
  use("lewis6991/gitsigns.nvim")
  use("Olical/conjure")

  do -- lspconfig
    use("neovim/nvim-lspconfig")
    use("williamboman/mason.nvim")
    use("williamboman/mason-lspconfig.nvim")
    use("folke/neodev.nvim")
    use("nvimdev/lspsaga.nvim")
  end

  do -- nvim-cmp
    use("hrsh7th/nvim-cmp")
    use("hrsh7th/cmp-nvim-lsp")
    use("hrsh7th/cmp-buffer")
    use("hrsh7th/cmp-path")
    use("hrsh7th/cmp-cmdline")
    use("saadparwaiz1/cmp_luasnip")
    use("PaterJason/cmp-conjure")
  end

  do -- eyecandy
    use("nvim-lualine/lualine.nvim")
    use("stevearc/dressing.nvim")
    use("nvimdev/dashboard-nvim", {
      event = "VimEnter"
    })
  end

  do -- themes
    use("blazkowolf/gruber-darker.nvim")
    use("nyoom-engineering/oxocarbon.nvim")
    use("sainnhe/everforest")
    use("EdenEast/nightfox.nvim")
    use("rose-pine/neovim")
  end
end)

require("nvim-treesitter.configs").setup({
  ensure_installed = {"c", "lua", "vim", "vimdoc", "query"},
  sync_install = false,
  auto_install = true,
  ignore_install = {},

  highlight = {
    enable = true
  }
})
require("which-key").setup()
--require("ibl").setup()
require("gitsigns").setup()
require("luasnip.loaders.from_snipmate").lazy_load()

require("mason").setup()
require("pyu.plugins.cmp")
require("pyu.plugins.lsp")

require("lualine").setup({})
require("dressing").setup({})
require("dashboard").setup({
  theme = "hyper",
  hide = {
    statusline = true,
    tabline = true,
    winbar = true
  },
  shortcut_type = "number"
})
