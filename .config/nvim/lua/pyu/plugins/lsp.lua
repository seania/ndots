local servers = {
  clangd = {},
  lua_ls = {},
  rust_analyzer = {},
}

require("mason-lspconfig").setup({
  ensure_installed = (function()
    local l = {}
    for k, _ in pairs(servers) do
      table.insert(l, k)
    end
    return l
  end)()
})
require("neodev").setup({})

local lspconfig = require("lspconfig")

for k, v in pairs(servers) do
  local opts = v or {}
  opts["capabilities"] = require("cmp_nvim_lsp").default_capabilities()

  lspconfig[k].setup(opts)
end

require("lspsaga").setup({})
