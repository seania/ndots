#!/usr/bin/env sh

echo "Pyu's Arch Setup Script"

if [[ $(whoami) == "root" ]]; then
    echo "DO NOT RUN AS ROOT!"
    echo "This script changes your shell which will mess up your system if you run as root."
    exit
fi

# Install main packages 
echo "Installing main packages (man, git, stow, etc.)"
sudo pacman -S git man-db stow vim wget curl grep

# Bluetooth
echo "Setting up Bluetooth"
sudo pacman -S bluez bluez-utils blueman
sudo systemctl enable bluetooth
sudo systemctl start bluetooth

# Setup Zsh
echo "Setting up Zsh"
sudo pacman -S zsh
chsh -s $(which zsh)

# Setup Yay AUR Helper
echo "Installing Yay"
git clone https://aur.archlinux.org/yay.git $HOME/yay
cd $HOME/yay
makepkg -si
cd $HOME
rm -rf $HOME/yay

# Setup Hyprland
echo "Installing Hyprland"
sudo pacman -S hyprland hypridle hyprlock hyprpaper hyprcursor swaync

# Setup Desktop Experience
echo "Setting up Desktop Exerperience"
sudo pacman -S waybar network-manager-applet brightnessctl wireplumber rofi-wayland wl-clipboard grim slurp swappy udiskie playerctl

# Install Programs
echo "Installing Primary Programs"
sudo pacman -S alacritty emacs-nativecomp thunar firefox neovim neovide tmux yazi

# Install Fonts
echo "Installing Fonts"
sudo pacman -S ttf-iosevka-nerd noto-fonts-cjk

# Setup Programming Environment
echo "Setting up Programming Environment"
sudo pacman -S tcc make cmake pkg-config # Minimal C Setup
sudo pacman -S sbcl roswell # Common Lisp
roswell # Install Roswell SBCL
roswell install asdf # Install ASDF
sudo pacman -S clojure leiningen # Clojure
sudo pacman -S ghc cabal-install # Haskell
sudo pacman -S go gopls # Go

# Setup GTK Themes and Cursors
yay -S bibata-cursor-theme-bin 

# CLI Toys
sudo pacman -S cmatrix cbonsai figlet

# Other tools
sudo pacman -S ffmpeg yt-dlp mpd ncmpcpp

# Clone and Setup Dotfiles

setupdots(){
 git clone https://gitlab.com/IamPyu/ndots $HOME/dotfiles
 cd $HOME/dotfiles
 stow .
 cd $HOME
}

[[ $1 = "setupdots" ]] && setupdots
