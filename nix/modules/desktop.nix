{config, pkgs, inputs, ...}: {
  environment.systemPackages = with pkgs; [
    brightnessctl wireplumber rofi-wayland wl-clipboard grim slurp
    swappy udiskie playerctl waybar swaynotificationcenter hypridle hyprlock swww
    libnotify networkmanagerapplet blueman
    bibata-cursors gnome.adwaita-icon-theme libadwaita
    teams-for-linux discord steam
  ];

  xdg.portal = {
    enable = true;
    extraPortals = [
      pkgs.xdg-desktop-portal-gtk
    ];
  };

  fonts = {
    enableDefaultPackages = true;
    fontDir.enable = true;
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji

      (nerdfonts.override {
        fonts = [
          "JetBrainsMono"
          "Iosevka"
        ];
      }) 
    ];
  };

  programs.dconf.enable = true;
  programs.hyprland = {
    enable = true;
    xwayland.enable = true;
  };
}
