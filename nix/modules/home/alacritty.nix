{config, lib, pkgs, inputs, ...}: let
  family = "JetBrains Mono Nerd Font";
in {
  programs.alacritty = {
    enable = true;
    settings = {
      window.title = "Terminal";
      window.dynamic_title = false;
      font = {
        size = lib.mkForce 12;
        normal.family = lib.mkForce family;
        italic.family = lib.mkForce family;
        bold.family = lib.mkForce family;
      };
    };
  };
}
