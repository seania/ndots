{config, pkgs, inputs, ...}: {
  home.stateVersion = "24.05";
  imports = [
    ./yazi.nix
    ./gtk.nix
    ./alacritty.nix
  ];
  
  programs.git = {
    enable = true;
    userName = "IamPyu";
    userEmail = "pyucreates@outlook.com";
  };

  programs.rofi = {
    enable = true;
    package = pkgs.rofi-wayland;
    location = "center";
    terminal = "${pkgs.alacritty}/bin/alacritty";
  };
  programs.kitty.enable = true;
  
  programs.zsh = {
    enable = true;
    enableCompletion = true;
    autocd = true;
    defaultKeymap = "emacs";
    dotDir = ".config/zsh";
    initExtra = "source ${./files/rc.zsh}";
  };

  home.packages = with pkgs; [
    prismlauncher papermc # Minecraft
  ];

  programs.home-manager.enable = true;
}
