emulate sh -c 'source /etc/profile' # In case /etc/zsh/zprofile gets exploded for some reason lol

export PATH="$PATH:$HOME/dotfiles/scripts:$HOME/dotfiles/wmscripts"

export EDITOR="emacs"
export VISUAL="emacs"

[ -d "/usr/share/glib-2.0/schemas" ] && export GSETTINGS_SCHEMA_DIR="/usr/share/glib-2.0/schemas"
[ -d "$HOME/.local/bin" ] && export PATH="$PATH:$HOME/.local/bin"
[ -d "$HOME/.roswell/bin" ] && export PATH="$PATH:$HOME/.roswell/bin"
[ -f "$HOME/.cargo/env" ] && . "$HOME/.cargo/env"
[[ -d "/var/lib/flatpak/exports/share" && -d "$HOME/.local/share/flatpak/exports/share" ]] && export XDG_DATA_DIRS="$XDG_DATA_DIRS:/var/lib/flatpak/exports/share:$HOME/.local/share/flatpak/exports/share"

[ -z "$PS1" ] && return

# autoloads
autoload -U colors && colors
autoload -U promptinit && promptinit

rm -rf ${ZDOTDIR:-${HOME}}/.zcompdump* # remove zcompdumps
autoload -U compinit && compinit

# enable zsh history
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory

# prompt
name="%{$fg[green]%}%n%{$reset_color%}"
host="%{$fg[blue]%}%m%{$reset_color%}"
dir="%{$fg[green]%}%~%{$reset_color%}"
tail="\$ "

export PROMPT="[$name@$host $dir]$tail"
export PS2="$lambda"

# change terminal title
DISABLE_AUTO_TITLE="true"
function termTitle() {
  echo -n -e "\033]0;$@\007"
}
termTitle "Terminal"

# alias and functoins
alias tm="tmux" # tmux
alias ls="ls --color -lah"
alias rm="rm -rI" # safe rm 
alias cls="clear" 
alias reload="zsh -l"
alias fetch="fastfetch"

fetch
