{config, pkgs, lib, inputs, ...}: {
  gtk = {
    enable = true;
    iconTheme.name = "Adwaita";
  };

  # might as well put qt in gtk.nix even though they are not related lmao

  qt = {
    enable = true;
    platformTheme.name = "gtk3";
  };
}
