{config, pkgs, inputs, ...}: {
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  nixpkgs = {
    config = {
      allowUnfree = true;
    };
  };
}
