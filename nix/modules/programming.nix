{config, pkgs, inputs, ...}: {
  environment.systemPackages = with pkgs; [
    tinycc gcc gdb binutils gnumake cmake pkg-config libtool # C/C++
    roswell sbcl # Common Lisp
    rustup rust-analyzer # Rust :3
  ];
}
