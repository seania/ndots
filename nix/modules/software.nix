{config, pkgs, inputs, ...}: {
  imports = [
    ./desktop.nix
    ./programming.nix
  ];

  environment.systemPackages = with pkgs; [
    tmux ncmpcpp fastfetch libvterm # terminal

    # essentials
    emacs
    xfce.thunar
    firefox
    alacritty
    neovim
    git
    curl
    stow
    busybox file
  ];

  programs.zsh.enable = true;
  programs.firefox.enable = true;

  services.flatpak.enable = true;

  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [];
}
