#!/usr/bin/env sh
VOLUME="5"
BRIGHTNESS="10"

alias set_volume="wpctl set-volume -l 1.0 @DEFAULT_SINK@"

if [[ "$1" = "vol" ]]; then
  if [[ "$2" = "up" ]]; then
    set_volume "${VOLUME}%+"
  elif [[ "$2" = "down" ]]; then
    set_volume "${VOLUME}%-"
  elif [[ "$2" = "mute" ]]; then
    wpctl set-mute @DEFAULT_SINK@ toggle
  fi
elif [[ "$1" = "bright" ]]; then
  if [[ "$2" = "up" ]]; then
    brightnessctl s "+${BRIGHTNESS}%"
  elif [[ "$2" = "down" ]]; then
    brightnessctl s "${BRIGHTNESS}%-" 
  fi
fi
