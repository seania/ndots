#!/usr/bin/env sh

if [[ "$1" = "lock" ]]; then
  mpc pause
  hyprlock
elif [[ "$1" = "unlock" ]]; then
  mpc play
fi
